use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rand::Rng;

#[derive(Debug)]
pub struct Airport {
    pub capacity: usize,
    pub load: usize,
    pub id: usize,
    path_sampler: WeightedIndex<u64>,
    paths: Vec<usize>,
}

impl Airport {
    pub fn new(
        capacity: usize,
        load: usize,
        id: usize,
        paths: Vec<(usize, u64)>,
    ) -> Result<Airport, Error> {
        Ok(Airport {
            capacity,
            load,
            id,
            path_sampler: WeightedIndex::new(paths.iter().map(|x| x.1))?,
            paths: paths.iter().map(|x| x.0).collect(),
        })
    }
    pub fn random_path<R: Rng>(&self, rng: &mut R) -> usize {
        self.paths[self.path_sampler.sample(rng)]
    }
}

use std::collections::HashMap;
#[derive(Debug)]
pub struct Setup {
    pub airports: Vec<Airport>,
    pub ap_codes: HashMap<String, usize>,
    pub codes: Vec<String>,
}

#[derive(Debug)]
pub enum Error {
    IoError(std::io::Error),
    SerdeError(serde_json::Error),
    DupAp,
    BadApPath,
    SamplerError,
    BadWeight(f64),
    UnknownAp,
    BadWeightSum,
    _Pass,
}

impl std::convert::From<std::io::Error> for Error {
    fn from(x: std::io::Error) -> Self {
        Error::IoError(x)
    }
}

impl std::convert::From<rand::distributions::weighted::WeightedError> for Error {
    fn from(_x: rand::distributions::weighted::WeightedError) -> Self {
        Error::SamplerError
    }
}

impl std::convert::From<serde_json::Error> for Error {
    fn from(x: serde_json::Error) -> Self {
        Error::SerdeError(x)
    }
}
