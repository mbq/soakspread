use crate::shared::{Airport, Error, Setup};
use crate::tasks::Tasks;
use serde::Deserialize;
use std::collections::HashMap;
use std::convert::TryFrom;

//NOTE: Maybe may go up to 2^64-1, but lower to be on a safe side of
// numerical errors
const WEIGHT_MAX: f64 = 1_000_000_000.;

#[derive(Deserialize, Debug)]
struct IoAirport {
    id: String,
    load: usize,
    capacity: usize,
    paths: HashMap<String, f64>,
}

#[derive(Deserialize, Debug)]
struct Jobfile {
    data: Vec<IoAirport>,
    tasks: Tasks,
}

impl TryFrom<&Jobfile> for Setup {
    type Error = Error;
    fn try_from(x: &Jobfile) -> Result<Self, Self::Error> {
        //Shrink codes
        let mut ap_codes: HashMap<String, usize> = HashMap::new();
        let mut codes: Vec<String> = Vec::new();
        for (id, io_ap) in x.data.iter().enumerate() {
            codes.push(io_ap.id.clone());
            if let Some(_) = ap_codes.insert(io_ap.id.clone(), id) {
                //Duplicated airport names
                return Err(Error::DupAp);
            }
        }
        let mut airports: Vec<Airport> = Vec::new();
        let mut idd = 0;
        for io_ap in &x.data {
            let mut paths = Vec::new();
            let mut sw = 0.;
            for (code, &weight) in &io_ap.paths {
                let id = *ap_codes.get(code).ok_or(Error::BadApPath)?;
                sw += weight;
                if !weight.is_finite() || weight < 0. || weight > 1. {
                    return Err(Error::BadWeight(weight));
                } else if weight > 0. {
                    paths.push((id, (weight * WEIGHT_MAX) as u64));
                }
            }
            if (sw - 1.).abs() > 1e-9 {
                return Err(Error::BadWeightSum);
            }
            idd = idd + 1;
            airports.push(Airport::new(io_ap.capacity, io_ap.load, idd - 1, paths)?);
        }
        Ok(Setup {
            ap_codes,
            airports,
            codes,
        })
    }
}
pub fn load_stream<R: std::io::Read>(inpt: R) -> Result<(Setup, Tasks), Error> {
    let jobfile: Jobfile = serde_json::from_reader(inpt)?;
    Ok((Setup::try_from(&jobfile)?, jobfile.tasks))
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufReader;
    use std::io::Cursor;
    const SAMPLE_JSON: &'static [u8] =
    b"{\"data\":[{\"id\":\"EPWA\",\"load\":2,\"capacity\":10,\"paths\":{\"EGLL\":1}},{\"id\":\"EGLL\",\"load\":2,\"capacity\":10,\"paths\":{\"EPWA\":1}}],\"tasks\":[]}";

    #[test]
    fn read_simple_json() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let jobfile: Jobfile = serde_json::from_reader(buf).unwrap();
        //assert_eq!(jobfile.close[0], "EPWA");
        assert_eq!(jobfile.data[1].id, "EGLL");
        assert_eq!(*jobfile.data[1].paths.get("EPWA").unwrap(), 1.0);
        assert_eq!(jobfile.data[1].load, 2);
    }
    #[test]
    fn convert_simple_json() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let k = load_stream(buf).unwrap();
        assert_eq!(k.0.airports[0].capacity, 10);
    }
}
