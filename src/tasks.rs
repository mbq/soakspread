use crate::shared::Setup;
use crate::sim::{Outcome, State};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub enum Task {
    Nop,
    JustRun { seed: u64, iters: usize },
    CloseEach { seed: u64, iters: usize },
}

#[derive(Serialize, Debug, Eq, PartialEq)]
#[serde(tag = "task")]
pub enum Answer {
    Nop,
    JustRun {
        seed: u64,
        iters: usize,
        overloaded: Outcome,
    },
    CloseEach {
        seed: u64,
        iters: usize,
        overloaded: HashMap<String, Outcome>,
    },
}

impl Task {
    fn exec(&self, setup: &Setup) -> Answer {
        match self {
            Task::Nop => Answer::Nop,
            &Task::JustRun { seed, iters } => Answer::JustRun {
                seed,
                iters,
                overloaded: State::new(setup).iters(iters).seed(seed).simulate(),
            },
            &Task::CloseEach { seed, iters } => Answer::CloseEach {
                seed,
                iters,
                overloaded: setup
                    .codes
                    .par_iter()
                    .enumerate()
                    .map(|(to_close, code)| {
                        (
                            code.clone(),
                            State::new(setup)
                                .seed(seed)
                                .iters(iters)
                                .close(to_close)
                                .simulate(),
                        )
                    })
                    .collect(),
            },
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Tasks(Vec<Task>);

#[derive(Serialize, Debug)]
pub struct Answers(Vec<Answer>);

impl Tasks {
    pub fn exec(self, setup: &Setup) -> Answers {
        let Tasks(x) = self;
        Answers(x.par_iter().map(|x| x.exec(setup)).collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::io::load_stream;
    use std::io::BufReader;
    use std::io::Cursor;
    const SAMPLE_JSON: &'static [u8] =
    b"{\"data\":[{\"id\":\"EPWA\",\"load\":2,\"capacity\":10,\"paths\":{\"EGLL\":1}},{\"id\":\"EGLL\",\"load\":2,\"capacity\":10,\"paths\":{\"EPWA\":1}}],\"tasks\":[]}";

    #[test]
    fn nop_job() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let setup = load_stream(buf).unwrap().0;
        assert_eq!(Task::Nop.exec(&setup), Answer::Nop);
    }
    #[test]
    fn close_each_job() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let setup = load_stream(buf).unwrap().0;
        use serde::Serialize;
        if let Answer::CloseEach { seed, overloaded } = (Task::CloseEach { seed: 11 }).exec(&setup)
        {
            assert_eq!(seed, 11);
            assert_eq!(
                overloaded["EPWA"],
                Outcome::Convergent(vec!["EPWA".to_string()])
            );
            assert_eq!(
                overloaded["EGLL"],
                Outcome::Convergent(vec!["EGLL".to_string()])
            );
        } else {
            unreachable!();
        }
    }
    #[test]
    fn just_run_job() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let setup = load_stream(buf).unwrap().0;
        if let Answer::JustRun { seed, overloaded } = (Task::JustRun { seed: 11 }).exec(&setup) {
            assert_eq!(seed, 11);
            assert_eq!(overloaded, Outcome::Convergent(vec![]));
        } else {
            unreachable!();
        }
    }
    #[test]
    fn many_nops() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let setup = load_stream(buf).unwrap().0;
        let z = Tasks(vec![Task::Nop, Task::Nop]);
        match z.exec(&setup) {
            Answers(ans) => assert_eq!(ans, vec![Answer::Nop, Answer::Nop]),
        }
    }
}
