mod io;
mod shared;
mod sim;
mod tasks;

fn main() {
    use crate::io::load_stream;
    match load_stream(std::io::stdin()) {
        Ok((setup, tasks)) => {
            serde_json::to_writer(std::io::stdout(), &tasks.exec(&setup)).unwrap();
            ()
        }
        Err(e) => eprintln!("Error: {:?}", e),
    }
}
