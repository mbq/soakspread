use crate::shared::{Error, Setup};
use rand::prelude::*;
use rand_pcg::Pcg64;
use serde::Serialize;

#[derive(Debug)]
pub struct State<'a> {
    lc: Vec<(usize, usize)>,
    setup: &'a Setup,
    seed: u64,
    iter_left: usize,
}

#[derive(Debug, Serialize, PartialEq, Eq)]
pub enum Outcome {
    Divergent,
    Convergent(Vec<String>),
}

impl<'a> State<'a> {
    fn airports(&self) -> usize {
        self.lc.len()
    }
    pub fn new(setup: &Setup) -> State {
        State {
            lc: setup
                .airports
                .iter()
                .map(|x| (x.load, x.capacity))
                .collect(),
            setup,
            seed: 0,
            iter_left: 100,
        }
    }
    pub fn close(&mut self, id: usize) -> &mut Self {
        self.lc[id].1 = 0;
        self
    }
    pub fn seed(&mut self, seed: u64) -> &mut Self {
        self.seed = seed;
        self
    }
    pub fn iters(&mut self, iters: usize) -> &mut Self {
        self.iter_left = iters;
        self
    }
    pub fn close_code(&mut self, code: &String) -> Result<&mut Self, Error> {
        match self.setup.ap_codes.get(code) {
            Some(&id) => {
                self.close(id);
                Ok(self)
            }
            None => Err(Error::UnknownAp),
        }
    }
    fn soak(&mut self) -> bool {
        //Soak
        let mut to_spread = false;
        for (l, c) in self.lc.iter_mut() {
            if *l > *c {
                *l = *l - *c;
                *c = 0;
                to_spread = true;
            } else {
                *c = *c - *l;
                *l = 0;
            }
        }
        to_spread
    }
    fn spread<R: Rng>(&self, rng: &mut R) -> Vec<usize> {
        let mut nl: Vec<usize> = std::iter::repeat(0).take(self.airports()).collect();
        for ((load, _), ap) in self.lc.iter().zip(self.setup.airports.iter()) {
            //Use ap paths
            for _craft in 0..*load {
                let to = ap.random_path(rng);
                nl[to] += 1;
            }
        }
        nl
    }
    fn step<R: Rng>(&mut self, rng: &mut R) -> bool {
        if self.soak() {
            let nl = self.spread(rng);
            //Put new loads
            for ((l, _), &ln) in self.lc.iter_mut().zip(nl.iter()) {
                *l = ln;
            }
            false
        } else {
            true
        }
    }
    pub fn simulate(&mut self) -> Outcome {
        let mut rng = Pcg64::new(self.seed.into(), 234);
        loop {
            if self.iter_left == 0 {
                break Outcome::Divergent;
            }
            if self.step(&mut rng) {
                let overloaded: Vec<_> = self
                    .lc
                    .iter()
                    .enumerate()
                    .filter_map(|(e, &(_, c))| if c == 0 { Some(e) } else { None })
                    .map(|id| self.setup.codes[id].clone())
                    .collect();
                break Outcome::Convergent(overloaded);
            }
            self.iter_left -= 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::io::load_stream;
    use std::io::BufReader;
    use std::io::Cursor;
    const SAMPLE_JSON: &'static [u8] =
    b"{\"data\":[{\"id\":\"A\",\"load\":5,\"capacity\":10,\"paths\":{\"B\":0.5,\"C\":0.5}},{\"id\":\"B\",\"load\":3,\"capacity\":5,\"paths\":{\"C\":0.5,\"A\":0.5}},{\"id\":\"C\",\"load\":0,\"capacity\":7,\"paths\":{\"A\":1}}],\"tasks\":[]}";
    #[test]
    fn simple_sim() {
        let buf = BufReader::new(Cursor::new(SAMPLE_JSON));
        let k = load_stream(buf).unwrap().0;
        let mut state = State::new(&k);
        state.close_code(&"A".to_string()).unwrap();
        let s1 = state.simulate();
        if let Outcome::Convergent(ab) = s1 {
            assert_eq!(ab[0], "A");
            assert_eq!(ab[1], "B");
        } else {
            assert!(false);
        }
        let s2 = State::new(&k)
            .close_code(&"A".to_string())
            .unwrap()
            .close_code(&"C".to_string())
            .unwrap()
            .seed(10)
            .iters(100)
            .simulate();

        //let s2 = state.simulate();
        if let Outcome::Divergent = s2 {
            assert!(true);
        } else {
            assert!(false);
        }
    }
}
